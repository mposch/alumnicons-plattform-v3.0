/*
 * Extra typings definitions
 */

// Allow .json files imports
declare module '*.json';

declare var faker: any;

declare var Parse: any;

// SystemJS module definition
declare var module: NodeModule;
interface NodeModule {
  id: string;
}
