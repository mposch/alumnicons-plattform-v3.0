import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '../core/i18n.service';
import { MaintenanceComponent } from './maintenance.component';

const routes: Routes = [
  { path: 'maintenance', component: MaintenanceComponent, data: { title: extract('Maintenance Mode') } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class MaintenanceRoutingModule { }
