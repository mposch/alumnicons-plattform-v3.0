import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/fromPromise';
import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AlumniService {

  alumni: any[] = [];
  private Alumni = Parse.Object.extend('Alumni');

  constructor() {
    _.times(10, (i) => {
      this.alumni.push({
        id: i + 1,
        firstname: faker.name.firstName(),
        lastname: faker.name.lastName(),
        email: faker.internet.email(),
        address: faker.address.streetAddress()
      });
    });

    this._createAccessor('firstName');
    this._createAccessor('lastName');
    this._createAccessor('mailaddress');
    this._createAccessor('alternativemailaddress');
    this._createAccessor('telephone');
    this._createAccessor('alternativetelephone');
    this._createAccessor('address');
    this._createAccessor('alternativeaddress');
    this._createAccessor('linkedin');
    this._createAccessor('xing');
    this._createAccessor('lat');
    this._createAccessor('lng');

    this._createAccessor('location');
    this._createAccessor('iconsStart');
    this._createAccessor('iconsEnd');

    this._createAccessor('employer');
    this._createAccessor('job');
    this._createAccessor('jobLevel');
    this._createAccessor('jobSince');
    this._createAccessor('jobBranch');
    this._createAccessor('companySize');

    this._createAccessor('isClubMember');
    this._createAccessor('hub');
    this._createAccessor('alumniRole');
    this._createAccessor('toBeApproved');
    this._createAccessor('hidden');
    this._createAccessor('inviteSent');
    this._createAccessor('payment');

    // this.fetchAll()
    //   .then( (result: any) => {
    //     console.log('all alumni', result);
    //     result.forEach((item: any) => {
    //       console.log('firstname', item.get('firstName'));
    //     });
    //   });
  }

  getAlumni(): Observable<any[]> {
    return Observable.fromPromise(this.fetchAll()
      .then((result: any) => {
        const mappedAlumni: any[] = _.map(result, (item: any) => {
          return {
            firstname: item.get('firstName'),
            lastname: item.get('lastName'),
            email: item.get('mailaddress'),
            address: item.get('address')
          };
        });

        return mappedAlumni;
      }));
    // return Observable.of(this.alumni);
  }

  saveAlumni(alumn: any) {
    if (alumn.id) {
      this.alumni = this.alumni.map((val) => {
        if (val.id === alumn.id) {
          return alumn;
        } else {
          return val;
        }
      });
    } else {
      this.alumni.push(alumn);
    }

    console.log(this.alumni);
  }

  deleteAlumni(alumn: any) {
    this.alumni = this.alumni.filter((val) => val.id !== alumn.id);
  }

  fetchAll() {
    const query = new Parse.Query(this.Alumni);
    query.ascending('lastName');
    query.equalTo('hidden', false);
    query.limit(50);
    query.skip(10);
    return query.find();
  }

  private _createAccessor(name: string) {
    Object.defineProperty(this.Alumni.prototype, name, {
      get: function () {
        return this.get(name);
      },
      set: function (aValue) {
        this.set(name, aValue);
      }
    });
  }
}
