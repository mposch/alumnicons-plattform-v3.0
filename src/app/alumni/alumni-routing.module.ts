import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { AlumniComponent } from './alumni.component';

const routes: Routes = Route.withShell([
  { path: 'alumni', component: AlumniComponent, data: { title: extract('Alumni') } }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AlumniRoutingModule { }
