import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ButtonModule} from 'primeng/primeng';
import {DataTableModule, SharedModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';

import { AlumniComponent } from './alumni.component';
import { AlumniRoutingModule } from './alumni-routing.module';
import { CarService } from './service/car.service';
import { AlumniService } from './service/alumni.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    DataTableModule,
    SharedModule,
    DialogModule,
    HttpClientModule,
    AlumniRoutingModule,
  ],
  declarations: [AlumniComponent],
  providers: [
    CarService,
    AlumniService
  ]
})
export class AlumniModule { }
