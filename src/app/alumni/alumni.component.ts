import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import {Observable} from 'rxjs/Observable';

import { environment } from '../../environments/environment';
import {AlumniService} from './service/alumni.service';

@Component({
  selector: 'app-alumni',
  templateUrl: './alumni.component.html',
  styleUrls: ['./alumni.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlumniComponent implements OnInit {

  version: string = environment.version;

  displayDialog: boolean;

  alumn: any = {};

  selectedAlumn: any;

  newAlumn: boolean;

  alumni: Observable<any[]>;

  constructor(
    private alumniService: AlumniService,
  ) { }

  ngOnInit() {
    this.alumni = this.alumniService.getAlumni();

    this.alumniService.getAlumni()
      .subscribe((data) => {
        console.log('SUBSCRIPTION DATA', data);
      });
  }

  showDialogToAdd() {
    this.newAlumn = true;
    this.alumn = {};
    this.displayDialog = true;
  }

  save() {
    this.alumniService.saveAlumni(this.alumn);
    this.alumn = null;
    this.displayDialog = false;
  }

  delete() {
    this.alumniService.deleteAlumni(this.selectedAlumn);
    this.alumn = null;
    this.displayDialog = false;
  }

  onRowSelect(event: any) {
    this.newAlumn = false;
    this.alumn = this.cloneCar(event.data);
    this.displayDialog = true;
  }

  cloneCar(c: any): any {
    const car = {};
    for (let prop in c) {
      car[prop] = c[prop];
    }
    return car;
  }

}
